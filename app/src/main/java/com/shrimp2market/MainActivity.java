package com.shrimp2market;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends Activity {

       private Button buttonGuide;
       private Button buttonRestaurants;
       private Button buttonOrder;
       private Button buttonGallery;
       private Button buttonAbout;
       private Typeface myCustomFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Context context = this;

        AppData.getInstance().init(context);

        buttonGuide = (Button) findViewById(R.id.main_btnGuide);
        myCustomFont = Typeface.createFromAsset(getAssets(),"fonts/OpenSans-CondLight.ttf");
        buttonGuide.setTypeface(myCustomFont);
        buttonGuide.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String url = "http://www.oceanwise.ca/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });


        buttonRestaurants = (Button) findViewById(R.id.main_btnRestaurants);
        buttonRestaurants.setTypeface(myCustomFont);

        buttonRestaurants.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, RestaurantsActivity.class);
                startActivity(intent);
            }
        });

        buttonOrder = (Button) findViewById(R.id.main_btnOrder);
        buttonOrder.setTypeface(myCustomFont);
        buttonOrder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderActivity.class);
                startActivity(intent);
            }
        });
        buttonGallery = (Button) findViewById(R.id.main_btnGallery);
        buttonGallery.setTypeface(myCustomFont);
        buttonGallery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(context, GalleryActivity.class);
                startActivity(intent);
            }
        });
        buttonAbout = (Button) findViewById(R.id.main_btnAbout);
        buttonAbout.setTypeface(myCustomFont);

        buttonAbout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, AboutActivity.class);
                startActivity(intent);

            }

        });






    }







}
