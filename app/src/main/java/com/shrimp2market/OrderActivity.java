package com.shrimp2market;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalOAuthScopes;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.paypal.android.sdk.payments.ShippingAddress;
import com.shrimp2market.adapters.OrderAdapterDelivery;
import com.shrimp2market.adapters.OrderAdapterQuantity;
import com.shrimp2market.models.PayPalHelper;
import com.shrimp2market.models.Transaction;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Zlatibor on 6/14/2015.
 */
public class OrderActivity extends Activity {

    private EditText name;
    private EditText surname;
    private Spinner spinnerQuantity;
    private Spinner spinnerDelivery;
    private EditText address;
    private EditText telephone;
    private ImageButton btnPaypal;
    private TextView txtMessage;

    private Transaction tempTransaction;
    private static final int REQUEST_CODE_PAYMENT = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        // Start PayPal service
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, PayPalHelper.config);
        startService(intent);

        Typeface myCustomFont = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-CondLight.ttf");
        Button header_btnBack = (Button) findViewById(R.id.header_btnBack);
        header_btnBack.setTypeface(myCustomFont);
        header_btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        TextView header_page = (TextView) findViewById(R.id.header_page);
        header_page.setTypeface(myCustomFont);

        spinnerQuantity = (Spinner) findViewById(R.id.order_dropdown1);
        spinnerDelivery = (Spinner) findViewById(R.id.order_dropdown2);
        OrderAdapterQuantity adapterQuantity = new OrderAdapterQuantity(this);
        OrderAdapterDelivery adapterDelivery = new OrderAdapterDelivery(this);
        spinnerQuantity.setAdapter(adapterQuantity);
        spinnerDelivery.setAdapter(adapterDelivery);

        name = (EditText) findViewById(R.id.order_Name);
        name.setTypeface(myCustomFont);

        surname = (EditText) findViewById(R.id.order_Surname);
        surname.setTypeface(myCustomFont);

        address = (EditText) findViewById(R.id.order_Address);
        address.setTypeface(myCustomFont);

        telephone = (EditText) findViewById(R.id.order_Telephone);
        telephone.setTypeface(myCustomFont);

        txtMessage = (TextView) findViewById(R.id.textMessage);

        btnPaypal = (ImageButton) findViewById(R.id.order_paypal);
        btnPaypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();

                if (txtMessage.getVisibility() == View.VISIBLE)
                    return;

                tempTransaction = new Transaction();

                tempTransaction.setName(name.getText().toString());
                tempTransaction.setSurname(surname.getText().toString());
                tempTransaction.setAddress(address.getText().toString());
                tempTransaction.setPhone(telephone.getText().toString());
                tempTransaction.setQuantity(spinnerQuantity.getSelectedItem().toString());
                tempTransaction.setDeliveryTime(spinnerDelivery.getSelectedItem().toString());

                BigDecimal price = null;
                if (spinnerQuantity.getSelectedItemPosition() == 1)
                    price = new BigDecimal("128.08");
                else if (spinnerQuantity.getSelectedItemPosition() == 2)
                    price = new BigDecimal("252.24");
                else if (spinnerQuantity.getSelectedItemPosition() == 3)
                    price = new BigDecimal("367.56");
                else {
                    txtMessage.setVisibility(View.VISIBLE);
                    txtMessage.setText(getResources().getString(R.string.order_paypal_error));
                    return;
                }

                tempTransaction.setPrice(price.toPlainString());

                PayPalPayment payment = new PayPalPayment(price, "CAD", "Shrimp - " + spinnerQuantity.getSelectedItem().toString(), PayPalPayment.PAYMENT_INTENT_SALE);
                Intent intent = new Intent(OrderActivity.this, PaymentActivity.class);
                intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, PayPalHelper.config);
                intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
                startActivityForResult(intent, REQUEST_CODE_PAYMENT);
            }
        });
    }

    private void validateFields(){
        txtMessage.setVisibility(View.GONE);
        txtMessage.setText(getResources().getString(R.string.order_text_message));

        if (name.getText().toString().equals("")) {
            txtMessage.setVisibility(View.VISIBLE);
            name.setBackgroundResource(R.drawable.stylemessage);
        } else {
            name.setBackgroundResource(R.drawable.edittextstyle);
        }

        if (surname.getText().toString().equals("")) {
            txtMessage.setVisibility(View.VISIBLE);
            surname.setBackgroundResource(R.drawable.stylemessage);
        } else {
            surname.setBackgroundResource(R.drawable.edittextstyle);
        }

        if (spinnerQuantity.getSelectedItem().equals(0)) {
            txtMessage.setVisibility(View.VISIBLE);
            spinnerQuantity.setBackgroundResource(R.drawable.stylemessage);
        } else {
            spinnerQuantity.setBackgroundResource(R.drawable.edittextstyle);
        }

        if (spinnerDelivery.getSelectedItem().equals(0)) {
            txtMessage.setVisibility(View.VISIBLE);
            spinnerDelivery.setBackgroundResource(R.drawable.stylemessage);
        } else {
            spinnerDelivery.setBackgroundResource(R.drawable.edittextstyle);
        }

        if (address.getText().toString().equals("")) {
            txtMessage.setVisibility(View.VISIBLE);
            address.setBackgroundResource(R.drawable.stylemessage);
        } else {
            address.setBackgroundResource(R.drawable.edittextstyle);
        }

        if (telephone.getText().toString().equals("")) {
            txtMessage.setVisibility(View.VISIBLE);
            telephone.setBackgroundResource(R.drawable.stylemessage);

        } else {
            telephone.setBackgroundResource(R.drawable.edittextstyle);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Log.d("PayPal","PayPal Ok");
            txtMessage.setVisibility(View.VISIBLE);
            txtMessage.setText(getResources().getString(R.string.order_paypal_success));
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            tempTransaction.setPaypalIntent(confirm.getProofOfPayment().getIntent());
            tempTransaction.setPaypalDate(confirm.getProofOfPayment().getCreateTime());
            tempTransaction.setPaypalState(confirm.getProofOfPayment().getState());
            tempTransaction.setPaypalPaymentId(confirm.getProofOfPayment().getPaymentId());
            AppData.getInstance().addTransaction(tempTransaction);
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.d("PayPal","PayPal Canceled");
            txtMessage.setVisibility(View.VISIBLE);
            txtMessage.setText(getResources().getString(R.string.order_paypal_canceled));
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.d("PayPal","PayPal Invalid");
            txtMessage.setVisibility(View.VISIBLE);
            txtMessage.setText(getResources().getString(R.string.order_paypal_error));
        }
    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}









