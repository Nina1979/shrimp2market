package com.shrimp2market;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.shrimp2market.models.PhotoItem;
import com.shrimp2market.models.RestaurantListModel;
import com.shrimp2market.models.Transaction;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Zlatibor on 6/7/2015.
 */
public class AppData {
    private final String KEY_TRANSACTIONS = "transactions";

    private static AppData instance;
    private Context context;
    private ArrayList<RestaurantListModel> restaurants = new ArrayList<RestaurantListModel>();
    private ArrayList<PhotoItem> photos = new ArrayList<PhotoItem>();
    private boolean loadedData;
    private Boolean loadedPhoto;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ArrayList<Transaction> transactions = new ArrayList<Transaction>();


    public static AppData getInstance() {
        if (instance == null)
            instance = new AppData();
        return instance;
    }

    private AppData() {}

    public void init(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("MyPREFERENCES",Context.MODE_PRIVATE );
        editor = sharedPreferences.edit();
    }

    public void loadData() {
        if (loadedData)
            return;

        // Loading transactions from shared prefs
        try {
            JSONObject jsonObject = new JSONObject(sharedPreferences.getString(KEY_TRANSACTIONS,"{\"transactions\": []}"));
            JSONArray jsonTransactions = jsonObject.getJSONArray("transactions");
            for(int i = 0; i < jsonTransactions.length(); i++){
                JSONObject jsonTransaction = jsonTransactions.getJSONObject(i);
                transactions.add(new Transaction(jsonTransaction));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Reading json file as string
        InputStream is = context.getResources().openRawResource(R.raw.restaurants);
        try {
            JSONObject jObject = new JSONObject(IOUtils.toString(is));
            JSONArray jArray = jObject.getJSONArray("RestaurantName");
            for (int i = 0; i < jArray.length(); i++) {
                RestaurantListModel listmodel = new RestaurantListModel();
                listmodel.name = jArray.getJSONObject(i).getString("name");
                listmodel.address = jArray.getJSONObject(i).getString("address");



                restaurants.add(listmodel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(is);
        }
        loadedData = true;
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public ArrayList<RestaurantListModel> getRestaurants() {
        return restaurants;
    }

    public ArrayList<PhotoItem> getPhotos() {
        return photos;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }
    public void addTransaction(Transaction t){
        transactions.add(t);
        saveTransactions();
    }
    private void saveTransactions(){
        JSONArray jsonArray = new JSONArray();
        for(Transaction t: transactions){
            jsonArray.put(t.toJSON());
        }
        JSONObject obj = new JSONObject();
        try {
            obj.put("transactions", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        editor.putString(KEY_TRANSACTIONS, obj.toString());
    }
}






