package com.shrimp2market;

import android.content.Context;
import android.os.AsyncTask;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.shrimp2market.adapters.GalleryAdapter;
import com.shrimp2market.models.PhotoItem;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public  class FetchPhotosTask extends AsyncTask<String, Void, Void> {
    protected Void doInBackground(String... url) {
        if(AppData.getInstance().getPhotos().size() > 0)
            return null;

        // Getting access token
        HttpURLConnection accessTokenConnection = null;
        URL accessTokenUrl = null;
        String accessTokenContent = null;
        try {
            accessTokenUrl = new URL("https://graph.facebook.com/oauth/access_token?client_id=695659367205825&client_secret=30d023862d350936c9773398044b5c20&grant_type=client_credentials");
            accessTokenConnection = (HttpURLConnection) accessTokenUrl.openConnection();
            InputStream accessTokenStream = new BufferedInputStream(accessTokenConnection.getInputStream());
            accessTokenContent = IOUtils.toString(accessTokenStream, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(accessTokenConnection != null) accessTokenConnection.disconnect();
        }

        AccessToken token = new AccessToken(accessTokenContent.split("=")[1], "695659367205825", "100009560838293", null, null, null, null, null );
        GraphResponse graphResponse2 = GraphRequest.newGraphPathRequest(token, "383443958529504/photos", null).executeAndWait();

        try {
            JSONArray itemList = graphResponse2.getJSONObject().getJSONArray("data");
            for (int i = 0; i < itemList.length(); i++) {
                PhotoItem photoitem = new PhotoItem();
                JSONObject photo = itemList.getJSONObject(i);
                photoitem.pictureUrl = photo.getString("picture");
                new DownloadImageTask().execute(photoitem.pictureUrl);
                photoitem.sourceUrl = photo.getString("source");
                new DownloadImageTask().execute(photoitem.sourceUrl);
                AppData.getInstance().getPhotos().add(photoitem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}




