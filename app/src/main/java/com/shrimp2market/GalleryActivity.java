package com.shrimp2market;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.shrimp2market.adapters.GalleryAdapter;
import com.shrimp2market.models.PhotoItem;
import com.shrimp2market.utils.ImageCache;

public class GalleryActivity extends Activity {
    private GridView gridView;
    private GalleryAdapter adapter;
    private ImageView large;
    private View largeImageLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_gallery);

        Typeface myCustomFont = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-CondLight.ttf");
        Button header_btnBack = (Button) findViewById(R.id.header_btnBack);
        header_btnBack.setTypeface(myCustomFont);
        header_btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        TextView header_page = (TextView) findViewById(R.id.header_page);
        header_page.setTypeface(myCustomFont);

        gridView = (GridView) findViewById(R.id.gridView);
        largeImageLayout = findViewById(R.id.large);
        large = (ImageView) findViewById(R.id.image_details);

        adapter = new GalleryAdapter(this);
        gridView.setAdapter(adapter);
        ImageCache.adapter = adapter;

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if (largeImageLayout.getVisibility() == View.VISIBLE) {
                    largeImageLayout.setVisibility(View.GONE);
                } else {
                    largeImageLayout.setVisibility(View.VISIBLE);
                    PhotoItem item = (PhotoItem) parent.getItemAtPosition(position);
                    Bitmap imageLarge = ImageCache.get(item.sourceUrl);
                    large.setImageBitmap(imageLarge);
                }
            }
        });

        large.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                largeImageLayout.setVisibility(View.GONE);
            }
        });

        new FetchPhotosTask().execute();
    }
}