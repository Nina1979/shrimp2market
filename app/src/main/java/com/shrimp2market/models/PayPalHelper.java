package com.shrimp2market.models;

import android.util.Log;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.shrimp2market.BuildConfig;

public class PayPalHelper {
    static{
        PayPalHelper.config =  new PayPalConfiguration();
        PayPalHelper.config.clientId("ASKhFzFyl92htWrTkhHWKl92rXHkPUxrtMBthjQLwlD9BcSPNDQpMeOSW8hrkPZfiNglomo4K0BKNlWn");

        if (BuildConfig.DEBUG){
            Log.d("PayPal","Activating DEBUG mode");
            PayPalHelper.config.environment(PayPalConfiguration.ENVIRONMENT_SANDBOX);
        }else{
            Log.d("PayPal","Activating PRODUCTION mode");
            PayPalHelper.config.environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION);
        }
    }

    public static PayPalConfiguration config;
}
