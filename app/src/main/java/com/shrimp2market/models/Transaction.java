package com.shrimp2market.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Zlatibor on 6/15/2015.
 */
public class Transaction {

    private String name;
    private String surname;
    private String address;
    private String phone;
    private String quantity;
    private String deliveryTime;
    private String price;
    private String paypalPaymentId = "";
    private String paypalState;
    private String paypalDate;
    private String paypalIntent;

    public Transaction() {}

    public Transaction(JSONObject obj) {
        try {
            this.setName(obj.getString("name"));
            this.setSurname(obj.getString("surname"));
            this.setAddress(obj.getString("address"));
            this.setPhone(obj.getString("phone"));
            this.setQuantity(obj.getString("quantity"));
            this.setDeliveryTime(obj.getString("deliveryTime"));
            this.setPrice(obj.getString("price"));
            this.setPaypalPaymentId(obj.getString("paypalPaymentId"));
            this.setPaypalState(obj.getString("paypalState"));
            this.setPaypalDate(obj.getString("paypalDate"));
            this.setPaypalIntent(obj.getString("paypalIntent"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String toJSON() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", getName());
            jsonObject.put("surname", getSurname());
            jsonObject.put("address", getAddress());
            jsonObject.put("phone", getPhone());
            jsonObject.put("quantity", getQuantity());
            jsonObject.put("deliveryTime", getDeliveryTime());
            jsonObject.put("price", getPrice());
            jsonObject.put("paypalPaymentId", getPaypalPaymentId());
            jsonObject.put("paypalState", getPaypalState());
            jsonObject.put("paypalDate", getPaypalDate());
            jsonObject.put("paypalIntent", getPaypalIntent());

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPaypalPaymentId() {
        return paypalPaymentId;
    }

    public void setPaypalPaymentId(String paypalPaymentId) {
        this.paypalPaymentId = paypalPaymentId;
    }

    public String getPaypalState() {
        return paypalState;
    }

    public void setPaypalState(String paypalState) {
        this.paypalState = paypalState;
    }

    public String getPaypalDate() {
        return paypalDate;
    }

    public void setPaypalDate(String paypalDate) {
        this.paypalDate = paypalDate;
    }

    public String getPaypalIntent() {
        return paypalIntent;
    }

    public void setPaypalIntent(String paypalIntent) {
        this.paypalIntent = paypalIntent;
    }
}
