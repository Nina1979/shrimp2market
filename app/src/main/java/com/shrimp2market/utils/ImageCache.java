  /*
FacebookAlbumDemo - Example of how to display Facebook albums in an Android application
Copyright (C) 2010-2011 Hugues Johnson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software 
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package com.shrimp2market.utils;

  import android.graphics.Bitmap;

  import com.shrimp2market.adapters.GalleryAdapter;

  import java.util.HashMap;

  public abstract class ImageCache{
	private static HashMap<String,Bitmap> hashMap;
    public static GalleryAdapter adapter;
	
	public static synchronized Bitmap get(String imageUrl){
		if(hashMap==null){hashMap=new HashMap<String,Bitmap>();}
		return(hashMap.get(imageUrl));
	}

	public static synchronized void put(String imageUrl,Bitmap bitmap){
		if(hashMap==null){hashMap=new HashMap<String,Bitmap>();}
		hashMap.put(imageUrl,bitmap);
        if (adapter != null) adapter.notifyDataSetChanged();
	}
}