package com.shrimp2market;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.shrimp2market.utils.ImageCache;

import java.io.InputStream;

/**
 * Created by Zlatibor on 6/12/2015.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    private String url;

    protected Bitmap doInBackground(String... urls) {
        this.url = urls[0];
        Bitmap mIcon11 = null;

        try {
            InputStream in = new java.net.URL(this.url).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        ImageCache.put(url, result);
    }
}

