package com.shrimp2market;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Zlatibor on 6/16/2015.
 */

public class AboutActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Typeface myCustomFont = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-CondLight.ttf");
        Button header_btnBack = (Button) findViewById(R.id.header_btnBack);
        header_btnBack.setTypeface(myCustomFont);
        header_btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        TextView header_page = (TextView) findViewById(R.id.header_page);
        header_page.setTypeface(myCustomFont);

        TextView about_txtContent = (TextView) findViewById(R.id.about_txtContent);
        about_txtContent.setTypeface(myCustomFont);
        about_txtContent.setText(Html.fromHtml(getResources().getString(R.string.about_content)));
    }
}
