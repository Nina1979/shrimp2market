package com.shrimp2market.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.shrimp2market.R;

/**
 * Created by zveljkovic on 6/24/2015.
 */
public class OrderAdapterDelivery extends BaseAdapter {
    private final String[] delivery = new String[]{
            "Set your desirable delivery time",
            "9am-4pm", "4pm-10pm"};
    private Context context;
    private Typeface fontOpenSansCondLight;

    public OrderAdapterDelivery(Context context) {
        this.context = context;
        this.fontOpenSansCondLight = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-CondLight.ttf");
    }

    @Override
    public int getCount() {
        return delivery.length;
    }

    @Override
    public Object getItem(int position) {
        return delivery[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final OrderAdapterDeliveryHolder holder;
        View v = convertView;
        if (v == null) {
            v = inflater.inflate(R.layout.spinner_item2, null);
            holder = new OrderAdapterDeliveryHolder();
            holder.delivery = (TextView) v.findViewById(R.id.spinner2);
            v.setTag(holder);
        } else {
            holder = (OrderAdapterDeliveryHolder) v.getTag();
        }

        holder.delivery.setTypeface(this.fontOpenSansCondLight);
        holder.delivery.setHint("" + delivery[position]);

        return v;
    }

    static class OrderAdapterDeliveryHolder {
        TextView delivery;
    }
}


