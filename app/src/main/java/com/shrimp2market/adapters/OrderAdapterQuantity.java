package com.shrimp2market.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.shrimp2market.OrderActivity;
import com.shrimp2market.R;

/**
 * Created by zveljkovic on 6/24/2015.
 */
public class OrderAdapterQuantity extends BaseAdapter {
    private final String[] quantity = new String[]{
            "Set your quantity", "4 Lbs = $128.08 (Saved 10%)", "8 Lbs = $252.24 (Saved 10%)",
            "12lbs = $367.56 (Saved 10%)"};

    private Context context;
    private Typeface fontOpenSansCondLight;

    public OrderAdapterQuantity(Context context) {
        this.context = context;
        this.fontOpenSansCondLight = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-CondLight.ttf");
    }

    @Override
    public int getCount() {
        return quantity.length;
    }

    @Override
    public Object getItem(int position) {
        return quantity[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final OrderAdapterQuantityHolder holder;
        View v = convertView;
        if (v == null) {
            v = inflater.inflate(R.layout.spinner_item1, null);
            holder = new OrderAdapterQuantityHolder();
            holder.quantity = (TextView) v.findViewById(R.id.spinner1);
            v.setTag(holder);
        } else {
            holder = (OrderAdapterQuantityHolder) v.getTag();
        }
        holder.quantity.setTypeface(fontOpenSansCondLight);
        holder.quantity.setHint("" + quantity[position]);

        return v;
    }

    static class OrderAdapterQuantityHolder {
        TextView quantity;
    }
}


