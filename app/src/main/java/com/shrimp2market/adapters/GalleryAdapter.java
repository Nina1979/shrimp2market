/*
FacebookAlbumDemo - Example of how to display Facebook albums in an Android application
Copyright (C) 2010-2011 Hugues Johnson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software 
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package com.shrimp2market.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.shrimp2market.AppData;
import com.shrimp2market.R;
import com.shrimp2market.models.PhotoItem;
import com.shrimp2market.utils.ImageCache;

import java.util.ArrayList;

public class GalleryAdapter extends BaseAdapter {

    private ArrayList<PhotoItem> photos;
    private Context context;
    private ImageView image;


    public GalleryAdapter(Context context) {
        this.context = context;
        this.photos = AppData.getInstance().getPhotos();

    }


    @Override
    public int getCount() {
        return (this.photos.size());
    }

    @Override
    public Object getItem(int position) {
        return (this.photos.get(position));
    }

    @Override
    public long getItemId(int position) {
        return (position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.grid_item_layout, parent, false);
            holder = new ViewHolder();
            holder.thumb = (ImageView) convertView.findViewById(R.id.image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        PhotoItem photo = (PhotoItem) this.getItem(position);
        Bitmap imageThumb = ImageCache.get(photo.pictureUrl);
        holder.thumb.setImageBitmap(imageThumb);


        return convertView;
    }








    static class ViewHolder {
        ImageView thumb;


    }
}
