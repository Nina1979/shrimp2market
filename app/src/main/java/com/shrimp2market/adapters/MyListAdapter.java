package com.shrimp2market.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.shrimp2market.AppData;
import com.shrimp2market.R;
import com.shrimp2market.models.RestaurantListModel;

import java.util.ArrayList;

/**
 * Created by Zlatibor on 4/11/2015.
 */
public class MyListAdapter extends BaseAdapter {

    Context context;
    ArrayList<RestaurantListModel> data;



    public MyListAdapter(Context context) {
        this.context = context;
        this.data = AppData.getInstance().getRestaurants();

    }

    @Override
    public int getCount() {
        return AppData.getInstance().getRestaurants().size();
    }

    @Override
    public Object getItem(int position) {
        return AppData.getInstance().getRestaurants().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
       /* We inflate the xml which gives us a view */
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.name_mylist_item, parent, false);
            holder = new ViewHolder();
        /* Get the widget with id name which is defined in the xml of the row */
            holder.name = (TextView) convertView.findViewById(R.id.name_mylist_item_txtName);
            holder.address = (TextView) convertView.findViewById(R.id.name_mylist_item_txtAddress);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        RestaurantListModel model = (RestaurantListModel) this.getItem(position);
        /* Populate the row's xml with info from the item */
        holder.name.setText(model.name);
        holder.address.setText(model.address);
        return convertView;
    }








    static class ViewHolder {
        TextView name;
        TextView address;
    }
}

