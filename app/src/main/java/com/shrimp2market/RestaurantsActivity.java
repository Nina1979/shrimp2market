package com.shrimp2market;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.shrimp2market.adapters.MyListAdapter;

public class RestaurantsActivity extends Activity implements OnMapReadyCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurants);

        final Context context = this;
        AppData.getInstance().init(context);

        Typeface myCustomFont = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-CondLight.ttf");
        Button header_btnBack = (Button) findViewById(R.id.header_btnBack);
        header_btnBack.setTypeface(myCustomFont);
        header_btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        TextView header_page = (TextView) findViewById(R.id.header_page);
        header_page.setTypeface(myCustomFont);

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.restaurants_map);
        ListView listView = (ListView) findViewById(R.id.restaurants_list);
        if (AppData.getInstance().isOnline())
        {
            mapFragment.getView().setVisibility(View.VISIBLE);
            mapFragment.getMapAsync(this);
        }
        else {
            mapFragment.getView().setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            AppData.getInstance().loadData();
            MyListAdapter adapter = new MyListAdapter(this);
            listView.setAdapter(adapter);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        LatLng BUCA_YORKVILE = new LatLng(43.644, -79.400);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(BUCA_YORKVILE, 13));

        map.addMarker(new MarkerOptions()
                .title("BUCA YORKVILE")
                .snippet("604 King St W\n" +
                        "Toronto, ON M5V 1M5\n")
                .position(BUCA_YORKVILE));

        LatLng  DANDYLION= new LatLng(43.642, -79.426);
        map.addMarker(new MarkerOptions()
                .title("DANDYLION")
                .snippet("198 Queen St W, \n" +
                        "Toronto, ON M6J 1J6\n")
                .position(DANDYLION));

        LatLng YASU= new LatLng(43.662, -79.403);
        map.addMarker(new MarkerOptions()
                .title("YASU")
                .snippet("81 Harbord St, \n" +
                        "Toronto, ON M5S\n")
                .position(YASU));

        LatLng DAILO= new LatLng(43.655, -79.409);
        map.addMarker(new MarkerOptions()
                .title("DAILO")
                .snippet("503 College Street, \n" +
                        "Toronto, ON M6J 2J3\n")
                .position(DAILO));

        LatLng BORALIA= new LatLng(43.645, -79.419);
        map.addMarker(new MarkerOptions()
                .title("BORALIA")
                .snippet("59 Ossington Ave, \n" +
                        "Toronto, ON M6J 2Y9\n")
                .position(BORALIA));

        LatLng BYBLOS= new LatLng(43.647, -79.388);
        map.addMarker(new MarkerOptions()
                .title("BYBLOS")
                .snippet("11 Duncan St, \n" +
                        "Toronto, ON M5H 3G6\n")
                .position(BYBLOS));

        LatLng FLOR_DE_SAL= new LatLng(43.677, -79.407);
        map.addMarker(new MarkerOptions()
                .title("FLOR DE SAL")
                .snippet("501 Davenport Road,\n" +
                        "Toronto, ON, M4V 1B8\n")
                .position(FLOR_DE_SAL));

        LatLng BAR_FANCY= new LatLng(43.643, -79.421);
        map.addMarker(new MarkerOptions()
                .title("BAR FANCY")
                .snippet("1070 Queen St W,\n" +
                        "Toronto, ON, M6J 1H8\n")
                .position(BAR_FANCY));

        LatLng NANA= new LatLng(43.646, -79.408);
        map.addMarker(new MarkerOptions()
                .title("NANA")
                .snippet("785 Queen St W,\n" +
                        "Toronto, ON, M6J 1G1\n")
                .position(NANA));

        LatLng RASA= new LatLng(43.662, -79.404);
        map.addMarker(new MarkerOptions()
                .title("RASA")
                .snippet("7196 Robert Street, \n" +
                        "          Toronto, ON M5S 2K8\n")
                .position(RASA));

        LatLng LOS_COLIBRIS= new LatLng(43.647, -79.387);
        map.addMarker(new MarkerOptions()
                .title("LOS COLIBRIS")
                .snippet("7220 King St W, Toronto, ON M5H 1K4")
                .position(LOS_COLIBRIS));

        LatLng BRANCA= new LatLng(43.649, -79.439);
        map.addMarker(new MarkerOptions()
                .title("BRANCA")
                .snippet("1727 Dundas St W, \n" +
                        "Toronto, ON M6K 1V4\n")
                .position(BRANCA));

        LatLng YUNAGHI= new LatLng(43.660, -79.414);
        map.addMarker(new MarkerOptions()
                .title("YUNAGHI")
                .snippet("538 Manning Ave, \n" +
                        "Toronto, ON M6G 2V9\n")
                .position(YUNAGHI));

        LatLng MR_FLAMINGO= new LatLng(43.649, -79.424);
        map.addMarker(new MarkerOptions()
                .title("MR. FLAMINGO")
                .snippet("1265 Dundas St W, \n" +
                        "Toronto, ON M6J 1X6\n")
                .position(MR_FLAMINGO));

        LatLng COLETTE= new LatLng(43.642, -79.401);
        map.addMarker(new MarkerOptions()
                .title("COLETTE")
                .snippet("550 Wellington St W, \n" +
                        "Toronto, ON M5V 2V5\n")
                .position(COLETTE));

        LatLng LUCKEE= new LatLng(43.644, -79.392);
        map.addMarker(new MarkerOptions()
                .title("LUCKEE")
                .snippet("328 Wellington St W, \n" +
                        "Toronto, ON M5V 3T4\n")
                .position(LUCKEE));

        LatLng AMERICA= new LatLng(43.649, -79.379);
        map.addMarker(new MarkerOptions()
                .title("AMERICA")
                .snippet("325 Bay St. 31st floor\n" +
                        "Toronto, ON, M5H 3C2\n")
                .position(AMERICA));

        LatLng FAT_PASHA= new LatLng(43.673, -79.411);
        map.addMarker(new MarkerOptions()
                .title("FAT PASHA")
                .snippet("414 Dupont St, \n" +
                        "Toronto, ON M5R 1V9\n")
                .position(FAT_PASHA));

        LatLng MONTECITO= new LatLng(43.647, -79.390);
        map.addMarker(new MarkerOptions()
                .title("MONTECITO")
                .snippet("299 Adelaide St W, \n" +
                        "Toronto, ON M5V 1P7\n")
                .position(MONTECITO));


        LatLng PATOIS= new LatLng(43.651, -79.408);
        map.addMarker(new MarkerOptions()
                .title("PATOIS")
                .snippet("2794 Dundas St W, \n" +
                        "Toronto, ON M6J 1V1\n")
                .position(PATOIS));

    }
}

